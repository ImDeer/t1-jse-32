package t1.dkhrunina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.ServerAboutRequest;
import t1.dkhrunina.tm.dto.request.ServerVersionRequest;
import t1.dkhrunina.tm.dto.response.ServerAboutResponse;
import t1.dkhrunina.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}