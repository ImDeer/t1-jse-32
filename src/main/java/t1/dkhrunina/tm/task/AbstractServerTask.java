package t1.dkhrunina.tm.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}