package t1.dkhrunina.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.endpoint.ISystemEndpoint;
import t1.dkhrunina.tm.dto.request.ServerAboutRequest;
import t1.dkhrunina.tm.dto.request.ServerVersionRequest;
import t1.dkhrunina.tm.dto.response.ServerAboutResponse;
import t1.dkhrunina.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractClient implements ISystemEndpoint {

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

}