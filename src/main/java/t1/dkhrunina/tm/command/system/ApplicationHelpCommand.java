package t1.dkhrunina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.command.AbstractCommand;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    private static final String NAME = "help";

    @NotNull
    private static final String DESCRIPTION = "Show available actions.";

    @Override
    public void execute() {
        System.out.println("\n[HELP]");
        for (@NotNull final AbstractCommand command : getCommandService().getTerminalCommands())
            System.out.println(command);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}