package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all projects.";

    @Override
    public void execute() {
        System.out.println("[Clear project list]");
        getProjectService().clear(getUserId());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}