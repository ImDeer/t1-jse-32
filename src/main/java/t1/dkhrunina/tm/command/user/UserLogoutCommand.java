package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "u-logout";

    @NotNull
    private static final String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[User logout]");
        getAuthService().logout();
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}