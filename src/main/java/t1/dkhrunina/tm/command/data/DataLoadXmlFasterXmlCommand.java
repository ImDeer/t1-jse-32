package t1.dkhrunina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataLoadXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-xml-faster";

    @NotNull
    private static final String DESCRIPTION = "Load data from FasterXML XML file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[Load data from FasterXML XML]");
        final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
        @Nullable final String xml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}