package t1.dkhrunina.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error: index is incorrect");
    }

}